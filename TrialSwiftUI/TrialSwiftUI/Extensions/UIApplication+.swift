//
//  UIApplication+.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 10/07/2021.
//

import Foundation
import SwiftUI

extension UIApplication {
    func endEditing() {
        sendAction(#selector(UIResponder.resignFirstResponder), to: nil, from: nil, for: nil)
    }
}
