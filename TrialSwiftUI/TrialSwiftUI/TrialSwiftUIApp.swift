//
//  TrialSwiftUIApp.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 08/07/2021.
//

import SwiftUI

@main
struct TrialSwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            RootView()
        }
    }
}
