//
//  NetworkManager.swift
//  Trial
//
//  Created by Umar Maikano on 19/01/2021.
//

import Foundation
import Combine

enum searchType {
    case player
    case team
    case all
}

class NetworkManager: NetworkManagerType {
    let baseSearchUrl = "https://trials.mtcmobile.co.uk/api/football/1.0/search"
    
    // perform GET request
    func performSearchRequest<T: Decodable>(with qeuryItems: [URLQueryItem], and searchType: T.Type) -> AnyPublisher<T, Error> {
        
        var urlComponents = URLComponents(string: baseSearchUrl) ?? URLComponents()
        urlComponents.queryItems = qeuryItems
        
        var request = URLRequest(url: urlComponents.url!)
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        
        return URLSession.shared.dataTaskPublisher(for: request)
            .retry(2)
            .map { $0.data }
            .decode(type: T.self, decoder: JSONDecoder())
            .eraseToAnyPublisher()
    }
}
