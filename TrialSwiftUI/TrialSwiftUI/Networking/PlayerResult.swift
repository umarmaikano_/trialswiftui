//
//  Player.swift
//  Trial
//
//  Created by Umar Maikano on 19/01/2021.
//

import Foundation

struct PlayerResponse: Decodable {
    let result: PlayerResult
}

// MARK: - PlayerResult
struct PlayerResult: Decodable {
    let players : [Player]?
    let status : Bool
    let message : String
    let request_order : Int
    let searchType : String
    let searchString : String
    let minVer : String
    let serverAlert : String
}

// MARK: - Players
struct Player: Decodable {
    let playerID : String
    let playerFirstName : String
    let playerSecondName : String
    let playerNationality : String
    let playerAge : String
    let playerClub : String
}
