//
//  Teams.swift
//  Trial
//
//  Created by Umar Maikano on 19/01/2021.
//

import Foundation

struct TeamResponse: Decodable {
    let result: TeamResult
}


// MARK: - TeamResult
struct TeamResult: Decodable {
    let teams: [Team]?
    let status: Bool
    let message: String
    let requestOrder: Int
    let searchType, searchString, minVer, serverAlert: String

    enum CodingKeys: String, CodingKey {
        case teams, status, message
        case requestOrder = "request_order"
        case searchType, searchString, minVer, serverAlert
    }
}

// MARK: - Team
struct Team: Decodable {
    let teamID, teamName, teamStadium, isNation: String
    let teamNationality, teamCity: String
}
