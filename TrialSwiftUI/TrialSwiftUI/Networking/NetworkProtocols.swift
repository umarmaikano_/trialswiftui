//
//  NetworkProtocols.swift
//  Trial
//
//  Created by Umar Maikano on 20/01/2021.
//

import Foundation
import Combine

protocol SearchResultDelegate: AnyObject {
    func handlerError()
    func didFinishSearch<T: Decodable>(result: T)
}

protocol NetworkManagerType: AnyObject {
    func performSearchRequest<T: Decodable>(with qeuryItems: [URLQueryItem], and searchType: T.Type) -> AnyPublisher<T, Error>
}
