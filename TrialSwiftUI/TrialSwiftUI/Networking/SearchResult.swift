//
//  SearchResult.swift
//  Trial
//
//  Created by Umar Maikano on 19/01/2021.
//

import Foundation

//MARK: - Search result model for both player and team
struct SearchResult: Decodable {
    let result: Results
}

struct Results : Decodable {
    let players : [Player]?
    let teams : [Team]?
    let status : Bool?
    let message : String?
    let request_order : Int?
    let searchType : String?
    let searchString : String?
    let minVer : String?
    let serverAlert : String?
}
