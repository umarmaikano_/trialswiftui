//
//  FavPlayer.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 08/07/2021.
//

import Foundation
import RealmSwift

class FavPlayer: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var club: String = ""
    @objc dynamic var age: String = ""
    @objc dynamic var nationality: String = ""
    
    override static func primaryKey() -> String? {
        return "name"
    }
}

extension FavPlayer {
    convenience init(player: PlayerProfile) {
        self.init()
        self.name = player.name
        self.club = player.club
        self.age = player.age
        self.nationality = player.nationality
    }
}
