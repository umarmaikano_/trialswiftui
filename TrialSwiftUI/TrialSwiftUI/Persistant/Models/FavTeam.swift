//
//  FavTeam.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 08/07/2021.
//

import Foundation
import RealmSwift

class FavTeam: Object {
    @objc dynamic var name: String = ""
    @objc dynamic var city: String = ""
    @objc dynamic var stadium: String = ""
    @objc dynamic var nationality: String = ""
    
    override static func primaryKey() -> String? {
        return "name"
    }
}

extension FavTeam {
    convenience init(team: TeamProfile) {
        self.init()
        self.name = team.name
        self.city = team.city
        self.stadium = team.stadium
        self.nationality = team.nationality
    }
}
