//
//  PersistantLoader.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import Foundation
import RealmSwift

class PersistantLoader {
    static let shared = PersistantLoader()
    
    func saveFavourites(items: (teams: [TeamProfile], players: [PlayerProfile])) {
        savePlayers(players: items.players)
        saveTeams(teams: items.teams)
    }
    
    func fetchFavourites() -> (teams: [TeamProfile], players: [PlayerProfile]) {
        return (fetchTeams(), fetchPlayers())
    }
    
    func deleteFavourites(items: (teams: [TeamProfile], players: [PlayerProfile])) {
        deleteTeams(teams: items.teams)
        deletePlayers(players: items.players)
    }
    
    private func fetchPlayers() -> [PlayerProfile] {
        do {
            
            let realm = try Realm()
            
            return realm.objects(FavPlayer.self).map { player -> PlayerProfile in
                return .init(name: player.name, age: player.age, club: player.club, nationality: player.nationality)
            }
            
        } catch let error {
            print(error.localizedDescription)
            return []
        }
    }
    
    private func fetchTeams() -> [TeamProfile] {
        do {
            
            let realm = try Realm()
            
            return realm.objects(FavTeam.self).map { team -> TeamProfile in
                return .init(name: team.name, city: team.city, stadium: team.stadium, nationality: team.nationality)
            }
            
        } catch let error {
            print(error.localizedDescription)
            return []
        }
    }
    
    private func savePlayers(players: [PlayerProfile]) {
        for player in players {
            do {
                let realm = try Realm()
                
                if !(realm.objects(FavPlayer.self).first(where: { $0.name == player.name }) != nil) {
                    let favePlayer = FavPlayer(player: player)
                    
                    try realm.write {
                        realm.add(favePlayer)
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    private func saveTeams(teams: [TeamProfile]) {
        for team in teams {
            do {
                let realm = try Realm()
                
                if !(realm.objects(FavTeam.self).first(where: { $0.name == team.name }) != nil) {
                    let faveTeam = FavTeam(team: team)
                    
                    try realm.write {
                        realm.add(faveTeam)
                    }
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    private func deletePlayers(players: [PlayerProfile]) {
        for player in players {
            do {
                let realm = try Realm()
                
                // fetch the image from realm
                guard let favPlayer = realm.objects(FavPlayer.self).first(where: { $0.name == player.name }) else { return }
                
                try realm.write {
                    realm.delete(favPlayer)
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
    
    private func deleteTeams(teams: [TeamProfile]) {
        for team in teams {
            do {
                let realm = try Realm()
                
                // fetch the image from realm
                guard let favTeam = realm.objects(FavTeam.self).first(where: { $0.name == team.name }) else { return }
                
                try realm.write {
                    realm.delete(favTeam)
                }
                
            } catch let error {
                print(error.localizedDescription)
            }
        }
    }
}
