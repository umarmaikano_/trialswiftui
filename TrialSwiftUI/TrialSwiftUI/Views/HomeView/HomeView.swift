//
//  HomeView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 08/07/2021.
//

import SwiftUI

struct HomeView: View {
    @ObservedObject var viewModel: HomeSearchViewModel = HomeSearchViewModel()
    
    var body: some View {
        ZStack(alignment: .top) {
            GeometryReader { geo in
                VStack(spacing: 32) {
                    Spacer()
                    SearchBarView(text: $viewModel.searchString, isLoading: $viewModel.isLoading)
                        .frame(width: geo.size.width, height: geo.size.width * 0.1, alignment: .center)
                    HStack {
                        Text("Click on item to mark as favourite")
                        Spacer()
                    }.padding(.leading, 32)
                    
                    ButtonView(geo: geo, title: "Add to favourite", action: .constant {
                        PersistantLoader.shared.saveFavourites(items: viewModel.favouriteList)
                        viewModel.removeAllFav() 
                        viewModel.reload()
                        UIApplication.shared.endEditing()
                    })
                    
                    VStack {
                        ScrollView {
                            ForEach(viewModel.renderedItems, id: \.self) { cellItem in
                                
                                switch cellItem {
                                case .team(let club, let city, let stadium, let imageName, let nationality):
                                    TeamView(geo: geo, club: club, city: city, stadium: stadium, imageName: imageName, nationality: nationality, addToFavList: { team in viewModel.addTeamToFavList(team: team)}, removeFromFavList: { team in viewModel.removeTeam(team: team)})
                                case .player(let name, let age, let club, let imageName, let nationality):
                                   PlayerView(geo: geo, name: name, age: age, club: club, imageName: imageName, nationality: nationality, addToFavList: { player in viewModel.addPlayerToFavList(player: player)}, removeFromFavList: { player in
                                        viewModel.removePlayer(player: player)
                                    })
                                case .header(let title):
                                    HeaderView(geo: geo, title: title)
                                case .more(let title):
                                    MoreView(geo: geo, title: title) {
                                        if title == "Players" {
                                            viewModel.getPlayerSearchResult(with: viewModel.searchString)
                                        } else if title == "Teams" {
                                            viewModel.getTeamSearchResult(with: viewModel.searchString)
                                        }
                                    }
                                }
                            }.background(GeometryReader {
                                Color.clear.preference(key: ViewOffsetKey.self,
                                    value: -$0.frame(in: .named("scroll")).origin.y)
                            })
                            .onPreferenceChange(ViewOffsetKey.self) { _ in UIApplication.shared.endEditing() }
                        }.coordinateSpace(name: "scroll")
                    }
                    Spacer()
                }
            }
        }
    }
    
}

struct HomeView_Previews: PreviewProvider {
    static var previews: some View {
        HomeView()
    }
}
