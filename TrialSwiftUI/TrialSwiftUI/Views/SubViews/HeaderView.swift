//
//  HeaderView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct HeaderView: View {
    let geo: GeometryProxy
    var title: String
    var body: some View {
        HStack {
            Spacer()
            Text(title)
                .padding(.leading, 12)
                .frame(width: geo.size.width * 0.9, height: 44, alignment: title == "Empty result" ? .center : .leading)
                .background(title == "Empty result" ? Color.white : Color.gray)
                .foregroundColor(Color.black)
            Spacer()
        }
    }
}

struct HeaderView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geo in
            HeaderView(geo: geo, title: "Teams")
        }
    }
}
