//
//  MoreView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct MoreView: View {
    let geo: GeometryProxy
    let title: String
    @State var action: () -> Void
    
    var body: some View {
        HStack {
            Button(action: action, label: {
                Text("More " + title)
            })
            .frame(width: geo.size.width * 0.3, height: 30, alignment: .center)
            .background(Color.blue)
            .foregroundColor(Color.black)
        }
        .frame(width: geo.size.width , height: 64, alignment: .center)
    }
}
