//
//  AddFavouriteView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct ButtonView: View {
    let geo: GeometryProxy
    let title: String
    
    @Binding var action: () -> Void
    
    var body: some View {
        HStack(alignment: .center) {
            Spacer()
            Button(action: {
                action()
            }, label: {
                Text(title)
            })
            .frame(width: geo.size.width * 0.4, height: 30, alignment: .center)
            .background(Color.gray.opacity(0.7))
            .foregroundColor(Color.black)
            Spacer()
        }
    }
}

struct AddFavouriteView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader {
            geo in
            ButtonView(geo: geo, title: "title", action: .constant {
            })
        }
    }
}
