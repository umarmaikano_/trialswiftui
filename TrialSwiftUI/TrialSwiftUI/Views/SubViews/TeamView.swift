//
//  TeamView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct TeamView: View {
    let geo: GeometryProxy
    var club: String
    var city: String
    var stadium: String
    var imageName: String
    var nationality: String
    
    @State var isChecked: Bool = false
    @State var addToFavList: (TeamProfile) -> Void
    @State var removeFromFavList: (TeamProfile) -> Void
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                HStack(spacing: 70) {
                        VStack(spacing: 12) {
                            Text(club)
                            Text(city)
                            Text(stadium)
                        }
                    HStack {
                        Image(imageName)
                            .resizable()
                            .frame(width: 50, height: 50, alignment: .trailing)
                        
                            switch isChecked {
                            case true:
                                Image(systemName: "checkmark").foregroundColor(.blue)
                            case false:
                                EmptyView()
                            }
                    }
                }
                .fixedSize()
                .frame(width: geo.size.width * 0.8, height: 100, alignment: .center)
                Spacer()
            }
            .frame(width: geo.size.width * 0.8, height: 100, alignment: .center)
            
            Divider()
        }
        .onTapGesture {
            if isChecked {
                removeFromFavList(TeamProfile(name: club, city: city, stadium: stadium, nationality: nationality))
            } else {
                addToFavList(TeamProfile(name: club, city: city, stadium: stadium, nationality: nationality))
            }
            
            isChecked = !isChecked
            UIApplication.shared.endEditing()
        }
    }
}

//struct TeamView_Previews: PreviewProvider {
//    static var previews: some View { GeometryReader { geo in
//        TeamView(geo: geo) }
//    }
//}
