//
//  PlayerView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct PlayerView: View {
    let geo: GeometryProxy
    var name: String
    var age: String
    var club: String
    var imageName: String
    var nationality: String
    
    @State var isChecked: Bool = false
    @State var addToFavList: (PlayerProfile) -> Void
    @State var removeFromFavList: (PlayerProfile) -> Void
    
    var body: some View {
        VStack {
            HStack {
                Spacer()
                HStack(spacing: 70) {
                    HStack(spacing: 15) {
                        VStack(alignment: .leading, spacing: 12) {
                            Text("name: " + name)
                            Text("Age: " + age)
                        } .padding(.leading,0)
                        .frame(width: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/, height: /*@START_MENU_TOKEN@*/100/*@END_MENU_TOKEN@*/)
                        .fixedSize(horizontal: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, vertical: false)
                        .onTapGesture(perform: {
                            if isChecked {
                                removeFromFavList(PlayerProfile(name: name, age: age, club: club, nationality: nationality))
                            } else {
                                addToFavList(PlayerProfile(name: name, age: age, club: club, nationality: nationality))
                            }
                            
                            isChecked = !isChecked
                            UIApplication.shared.endEditing()
                        })
                        
                        Text("Club: " + club)
                            .frame(width: 100, height: 100, alignment: .center)
                            .fixedSize(horizontal: /*@START_MENU_TOKEN@*/true/*@END_MENU_TOKEN@*/, vertical: false)
                    }
                    HStack {
                        Image(imageName)
                            .resizable()
                            .frame(width: 50, height: 50, alignment: .trailing)
            
                            switch isChecked {
                            case true:
                                Image(systemName: "checkmark").foregroundColor(.blue)
                            case false:
                                EmptyView()
                            }
                    }
                }
                .fixedSize()
                .frame(width: geo.size.width * 0.8, height: 100, alignment: .center)
                Spacer()
            }
            .frame(width: geo.size.width * 0.8, height: 100, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            Divider()
        } .onTapGesture {
            if isChecked {
                removeFromFavList(PlayerProfile(name: name, age: age, club: club, nationality: nationality))
            } else {
                addToFavList(PlayerProfile(name: name, age: age, club: club, nationality: nationality))
            }
            
            isChecked = !isChecked
            UIApplication.shared.endEditing()
        }
    }
}

struct PlayerView_Previews: PreviewProvider {
    static var previews: some View {
        GeometryReader { geo in
            PlayerView(geo: geo, name: "name", age: "age", club: "club", imageName: "aland", nationality: "", addToFavList: { _ in }, removeFromFavList: { _ in })
        }
    }
}
