//
//  SearchBarView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI
import UIKit

struct SearchBarView: View {
    @Binding var text: String
    @Binding var isLoading: Bool
    
    var body: some View {
        ZStack {
            GeometryReader { geometry in
                HStack() {
                    Spacer()
                    Image(systemName: "magnifyingglass")
                        .resizable()
                        .frame(width: geometry.size.width * 0.1, height: geometry.size.width * 0.1, alignment: .leading)
                    
                    TextField("Teams & players search text", text: $text)
                        .frame(width: geometry.size.width * 0.8, height: geometry.size.width * 0.1, alignment: .leading)
                        .border(width: 2, edges: [.bottom], color: .gray)
                        .overlay(ZStack{
                            switch isLoading {
                            case true: ProgressView()
                            case false: EmptyView()
                            }
                        }, alignment: .trailing)
                    Spacer()
                }
            }
        }
    }
}



struct SearchBarView_Previews: PreviewProvider {
    static var previews: some View {
        SearchBarView(text: .constant(""), isLoading: .constant(true))
    }
}
