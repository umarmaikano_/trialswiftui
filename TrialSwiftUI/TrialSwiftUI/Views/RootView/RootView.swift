//
//  RootView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct RootView: View {
    var body: some View {
        TabView{
            HomeView()
                .tabItem{
                    Image(systemName:"list.bullet.rectangle")
                    Text("Items")
                }
            FavView()
                .tabItem{
                    Image(systemName:"star")
                    Text("Saved")
                }
        }
    }
}

struct RootView_Previews: PreviewProvider {
    static var previews: some View {
        RootView()
    }
}
