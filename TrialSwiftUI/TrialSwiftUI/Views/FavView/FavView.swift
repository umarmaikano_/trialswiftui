//
//  FavView.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 09/07/2021.
//

import SwiftUI

struct FavView: View {
    @ObservedObject var viewModel: FavListViewModel = FavListViewModel()
    
    var body: some View {
        ZStack(alignment: .top) {
            GeometryReader { geo in
                VStack(spacing: 32) {
                    Spacer()
                    
                    ButtonView(geo: geo,title: "Delete", action: .constant {
                        PersistantLoader.shared.deleteFavourites(items: viewModel.favouriteListToDelete)
                        viewModel.removeAllFavToDelete()
                        viewModel.reload()
                    })
                    
                    HStack {
                        Text("Click on item to check and press delete")
                        Spacer()
                    }.padding(.leading, 32)
                    
                    VStack {
                        ScrollView {
                            ForEach(viewModel.renderedItems, id: \.self, content: { cellItem in
                                
                                switch cellItem {
                                case .team(let club, let city, let stadium, let imageName, let nationality):
                                    TeamView(geo: geo, club: club, city: city, stadium: stadium, imageName: imageName, nationality: nationality, addToFavList: { team in viewModel.addTeamToFavListToDelete(team: team)}, removeFromFavList: { team in viewModel.removeTeamToDelete(team: team)})
                                case .player(let name, let age, let club, let imageName, let nationality):
                                    PlayerView(geo: geo, name: name, age: age, club: club, imageName: imageName, nationality: nationality, addToFavList: { player in viewModel.addPlayerToFavListToDelete(player: player)}, removeFromFavList: { player in
                                        viewModel.removePlayerToDelete(player: player)
                                    })
                                case .header(let title):
                                    HeaderView(geo: geo, title: title)
                                case .more: EmptyView()
                                }
                                
                                EmptyView()
                            }
                            )
                        }
                        .onAppear {
                            viewModel.getFavList()
                        }
                    }
                    Spacer()
                }
            }
        }
    }
}

struct FavView_Previews: PreviewProvider {
    static var previews: some View {
        FavView()
    }
}
