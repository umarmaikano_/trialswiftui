//
//  ViewOffSet.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 10/07/2021.
//

import Foundation
import SwiftUI

struct ViewOffsetKey: PreferenceKey {
    typealias Value = CGFloat
    static var defaultValue = CGFloat.zero
    static func reduce(value: inout Value, nextValue: () -> Value) {
        value += nextValue()
    }
}
