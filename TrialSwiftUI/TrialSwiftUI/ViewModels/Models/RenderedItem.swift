//
//  RenderedItem.swift
//  TrialSwiftUI
//
//  Created by Umar Maikano on 10/07/2021.
//

import Foundation

enum RenderCellItem: Hashable {
    case player (name: String, age: String, club: String, imageName: String, nationality: String)
    case team (club: String, city: String, stadium: String, imageName: String, nationality: String)
    case header (title: String)
    case more (title: String)
}
