//
//  FavListModels.swift
//  Trial
//
//  Created by Umar Maikano on 20/01/2021.
//

import Foundation
// favourite table view cell item model
struct FavCellItem {
    var marked: Bool = false
    let content: FavItem
}

enum FavItem {
    case header(itemType)
    case player(FavPlayer)
    case team(FavTeam)
}
