//
//  HomeSearchModel.swift
//  Trial
//
//  Created by Umar Maikano on 20/01/2021.
//

import Foundation

// cell Item on homesearch tableview
struct CellItem: Hashable {
    var marked: Bool = false
    let content: TableViewItemType
    
    mutating func unMark() {
        self.marked = false
    }
    
    func renderItem() -> RenderCellItem {
        switch self.content {
        case .player(let player):
            let imageName = K.playNationalityFlagMap[player.nationality] ?? ""
            return .player(name: player.name, age: player.age, club: player.club, imageName: imageName, nationality: player.nationality)
        case .team(let team):
            let imageName = K.teamNationalityFlagMap[team.nationality] ?? ""
            return .team(club: team.name, city: team.city, stadium: team.stadium, imageName: imageName, nationality: team.nationality)
        case .emptyResult:
            return .header(title: "Empty result")
        case .header(let type):
            switch type {
            case .player:
                return .header(title: "Players")
            case .team:
                return .header(title: "Teams")
            }
        case .actionButton(let type, _):
            switch type {
            case .player:
                return .more(title: "Players")
            case .team:
                return .more(title: "Teams")
            }
        }
    }
}

// cell item as either header, emptyresult, team, player or actionButton
enum TableViewItemType: Hashable {
    case header(itemType)
    case player(PlayerProfile)
    case team(TeamProfile)
    case actionButton(itemType, () -> Void )
    case emptyResult(searchType)
    
    func hash(into hasher: inout Hasher) { }
    
    static func == (lhs: TableViewItemType, rhs: TableViewItemType) -> Bool {
        switch (lhs, rhs) {
        default:
            return false
        }
    }
}

// player or team item enum
enum itemType {
    case player
    case team
}

// player and team profile model
struct PlayerProfile: Equatable {
    let name: String
    let age: String
    let club: String
    let nationality: String
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.name == rhs.name && lhs.age == rhs.age && lhs.club == rhs.club
    }
}

struct TeamProfile: Equatable {
    let name: String
    let city: String
    let stadium: String
    let nationality: String
    
    static func == (lhs: Self, rhs: Self) -> Bool {
        return lhs.name == rhs.name && lhs.city == rhs.city && lhs.stadium == rhs.stadium
    }
}
