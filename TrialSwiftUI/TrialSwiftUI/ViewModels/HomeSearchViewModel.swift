//
//  HomeSearchViewModel.swift
//  Trial
//
//  Created by Umar Maikano on 19/01/2021.
//

import Foundation
import Combine

class HomeSearchViewModel: ObservableObject {
    var networkManager: NetworkManagerType!
    var items: [CellItem] = []
    var playerOffset = 10
    var teamOffset = 10
    var errorState: Bool = false
    var timer: Timer?
    var requestOrder: Int = 0
    @Published var searchString = "" {
        didSet {
            if timer != nil {
                timer?.invalidate()
            }
            
            // timer for 2.5 seconds
            timer = Timer.scheduledTimer(withTimeInterval: 0.25, repeats: false) { [weak self] timer  in
                
                if let searchString = self?.searchString {
                    self?.requestOrder += 1
                    self?.getSearchResult(with: searchString)
                }
            }
        }
    }
    
    @Published var renderedItems: [RenderCellItem] = []
    
    @Published var favouriteList: (teams: [TeamProfile], players: [PlayerProfile]) = (teams: [], players: [])
    @Published var isLoading: Bool = false
    
    var favPlayers: [PlayerProfile] = []
    var favTeams: [TeamProfile] = []
    
    var subscriptions = Set<AnyCancellable>()
    
    init() {
       self.networkManager = NetworkManager()
    }
    
    // MARK:- mutating functions
    
    func insertPlayersAndTeams(players: [PlayerProfile]?, teams: [TeamProfile]?) {
        items.removeAll()
        // no result found for player and team
        guard !(players == nil && teams == nil) else {
            items.append(.init(content: .emptyResult(.all)))
            renderedItems = items.map { item in return item.renderItem() }
            return
        }
        
        // append player result
        if let players = players {
            items.append(.init(content: .header(.player)))
            for player in players {
                items.append(.init(content: .player(player)))
            }
            
            if players.count == 10 {
                items.append(.init(content: .actionButton(.player, {
                    self.getPlayerSearchResult(with: self.searchString )
                })))
            }
        }
        
        // apend team result
        if let teams = teams {
            items.append(.init(content: .header(.team)))
            for team in teams {
                items.append(.init(content: .team(team)))
            }
            
            if teams.count == 10 {
                items.append(.init(content: .actionButton(.team, {
                    self.getTeamSearchResult(with: self.searchString )
                })))
            }

        }
        
        // no team result found
        if teams == nil {
            items.append(.init(content: .emptyResult(.team)))
        }
        
        // no player result found
        if players == nil {
            items.append(.init(content: .emptyResult(.player)))
        }
        
        playerOffset = 10
        teamOffset = 10
    }
    
    func insertMorePlayerItems(playersItems: [CellItem]) {
        if let index = items.firstIndex(where: { if case .player = $0.content { return true }; return false } ) {
            items.insert(contentsOf: playersItems, at: index)
        }
    }
    
    func insertMoreTeamItems(teamsItems: [CellItem]) {
        if let index = items.firstIndex(where: { if case .team = $0.content { return true }; return false } ) {
            items.insert(contentsOf: teamsItems, at: index)
        }
    }
    
    func removeMorePlayerButton() {
        items.removeAll(where: {
            if case .actionButton(.player,_) = $0.content {
                return true
            }
            return false
        })
    }
    
    func removeMoreTeamsButton() {
        items.removeAll(where: {
            if case .actionButton(.team,_) = $0.content {
                return true
            }
            return false
        })
    }
    
    
    // MARK: - Helper funciton
    
    func playersToProfile(players: [Player]) -> [PlayerProfile] {
        return players.map { (player) -> PlayerProfile in
            return .init(name: player.playerFirstName, age: player.playerAge, club: player.playerClub, nationality: player.playerNationality )
        }
    }
    
    func teamsToProfile(teams: [Team]) -> [TeamProfile] {
        return teams.map { (team) -> TeamProfile in
            return .init(name: team.teamName, city: team.teamCity, stadium: team.teamStadium, nationality: team.teamNationality)
        }
    }
    
    func reload() {
        renderedItems.removeAll()
        getSearchResult(with: searchString)
    }
    

// MARK: - HomeSearchViewModelType implementation
    
    // search for items
    func getSearchResult(with searchString: String) {
        defer {
            self.searchString = searchString
            timer?.invalidate()
            timer = nil
        }
        isLoading = true
        networkManager.performSearchRequest(with:  [URLQueryItem(name: "searchString", value: searchString), URLQueryItem(name: "requestOrder", value: String(requestOrder))], and: SearchResult.self)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { [self] completion in
                switch completion {
                case .failure:
                    self.handlerError()
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { result in
                self.handleSearchResult(searchResult: result)
            })
            .store(in: &subscriptions)
    }
    
    // search for more player items
    func getPlayerSearchResult(with searchString: String) {
        self.isLoading = true
        networkManager.performSearchRequest(with: [URLQueryItem(name: "searchString", value: searchString), URLQueryItem(name: "searchType", value: "players"), URLQueryItem(name: "offset", value: String(playerOffset))], and: PlayerResponse.self)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure:
                    self.handlerError()
                    self.isLoading = false
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { result in
                self.handlePlayerSearch(playerResult: result.result)
            })
            .store(in: &subscriptions)
    }
    
    // search for more team items
    func getTeamSearchResult(with searchString: String) {
        self.isLoading = true
        networkManager.performSearchRequest(with: [URLQueryItem(name: "searchString", value: searchString), URLQueryItem(name: "searchType", value: "teams"), URLQueryItem(name: "offset", value: String(teamOffset))], and: TeamResponse.self)
            .receive(on: RunLoop.main)
            .sink(receiveCompletion: { completion in
                switch completion {
                case .failure:
                    self.handlerError()
                case .finished:
                    self.isLoading = false
                }
            }, receiveValue: { result in
                self.handleTeamSearch(teamResult: result.result)
            })
            .store(in: &subscriptions)
    }
    
    func addPlayerToFavList(player: PlayerProfile) {
        favPlayers.append(player)
        favouriteList = (teams: favTeams, players: favPlayers)
    }
    
    func addTeamToFavList(team: TeamProfile) {
        favTeams.append(team)
        favouriteList = (teams: favTeams, players: favPlayers)
    }
    
    func removePlayer(player: PlayerProfile) {
        favPlayers.removeAll(where: { $0 == player })
        favouriteList = (teams: favTeams, players: favPlayers)
    }
    
    func removeTeam(team: TeamProfile) {
        favTeams.removeAll(where: { $0 == team })
        favouriteList = (teams: favTeams, players: favPlayers)
    }
    
    func removeAllFav() {
        favTeams.removeAll()
        favPlayers.removeAll()
        favouriteList = (teams: favTeams, players: favPlayers)
    }
    
//MARK: - Network request handlers
    
    // player search only handler
    func handlePlayerSearch(playerResult: PlayerResult) {
        errorState = false
        
        // guard if no players where found
        guard let players = playerResult.players else {
            removeMorePlayerButton()
            renderedItems = items.map { item in return item.renderItem() }
            return
        }
        
        let playerProfiles = playersToProfile(players: players)
        var playerItems: [CellItem] = []
        
        for playerProfile in playerProfiles {
            playerItems.append(.init(content: .player(playerProfile)))
        }
        
        insertMorePlayerItems(playersItems: playerItems)
        
        // remove more button if less than 10 items where fetched
        if players.count < 10 {
            removeMorePlayerButton()
        }
        
        // render result on screen
        renderedItems = items.map({ item -> RenderCellItem in
            return item.renderItem()
        })
        
        playerOffset += 10
    }
    
    // team search only handler
    func handleTeamSearch(teamResult: TeamResult) {
        errorState = false
        
        // guard if no teams where found
        guard let teams = teamResult.teams else {
            removeMoreTeamsButton()
            renderedItems = items.map { item in return item.renderItem() }
            return
        }
        
        let teamProfiles = teamsToProfile(teams: teams)
        var teamsItems: [CellItem] = []
            
        for teamProfile in teamProfiles {
            teamsItems.append(.init(content: .team(teamProfile)))
        }
            
        insertMoreTeamItems(teamsItems: teamsItems)
        
        if teams.count < 10 {
            removeMoreTeamsButton()
        }
        
        // render result on screen
        renderedItems = items.map({ item -> RenderCellItem in
           return item.renderItem()
        })
        
        teamOffset += 10
    }
    
    // search result handler
    func handleSearchResult(searchResult: SearchResult) {
        var players: [PlayerProfile]?
        var teams: [TeamProfile]?
        
        if let playerResult = searchResult.result.players {
            players = playersToProfile(players: playerResult)
        }
        
        if let teamResult = searchResult.result.teams {
            teams = teamsToProfile(teams: teamResult)
        }
        
        insertPlayersAndTeams(players: players, teams: teams)
        
        // render result on screen
        renderedItems = items.map({ item -> RenderCellItem in
            return item.renderItem()
        })
    }
    
    // handle error state
    func handlerError() {
        
        // guard state not already in error
        guard !self.errorState else {
            return
        }
        
        items.removeAll()
        renderedItems = items.map { item in return item.renderItem() }
        self.errorState = true
    }
    
}
