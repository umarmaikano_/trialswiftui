//
//  FavListViewModel.swift
//  Trial
//
//  Created by Umar Maikano on 20/01/2021.
//

import Foundation

class FavListViewModel: ObservableObject {
    
    var favPlayers: [PlayerProfile] = []
    var favTeams: [TeamProfile] = []
    var favCellList: [CellItem] = []
    
    @Published var renderedItems: [RenderCellItem] = []
    
    @Published var favouriteListToDelete: (teams: [TeamProfile], players: [PlayerProfile]) = (teams: [], players: [])
    
    func getFavList() {
        favCellList.removeAll()
        
        let items = PersistantLoader.shared.fetchFavourites()
        
        if items.players.count > 0 {
            favCellList.append(.init(content: .header(.player)))
            
            for player in items.players {
                favCellList.append(.init(content: .player(player)))
            }
        }
        
        if items.teams.count > 0 {
            favCellList.append(.init(content: .header(.team)))
            
            for team in items.teams {
                favCellList.append(.init(content: .team(team)))
            }
        }
        
        renderedItems = favCellList.map { item in return item.renderItem() }
    }
    
    func reload() {
        renderedItems.removeAll()
        getFavList()
    }
    
    func addPlayerToFavListToDelete(player: PlayerProfile) {
        favPlayers.append(player)
        favouriteListToDelete = (teams: favTeams, players: favPlayers)
    }
    
    func addTeamToFavListToDelete(team: TeamProfile) {
        favTeams.append(team)
        favouriteListToDelete = (teams: favTeams, players: favPlayers)
    }
    
    func removePlayerToDelete(player: PlayerProfile) {
        favPlayers.removeAll(where: { $0 == player })
        favouriteListToDelete = (teams: favTeams, players: favPlayers)
    }
    
    func removeTeamToDelete(team: TeamProfile) {
        favTeams.removeAll(where: { $0 == team })
        favouriteListToDelete = (teams: favTeams, players: favPlayers)
    }
    
    func removeAllFavToDelete() {
        favTeams.removeAll()
        favPlayers.removeAll()
        favouriteListToDelete = (teams: favTeams, players: favPlayers)
    }
    
}
